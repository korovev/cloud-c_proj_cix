#!/usr/bin/env python

import os

IS_LOCAL = False
IS_PUBLIC_TEST = False
BACK_ADDR = "http://localhost:5001" if IS_LOCAL else "http://AWSomeLoadBalancer-1515858610.us-east-1.elb.amazonaws.com"
# Apparently the home option doesn't work
PATH = "" if IS_LOCAL else "/var/www/aws_flask"

DEBUG = True
DATA = "/tmp"
WORDS = os.path.join(PATH, "words")

# EMAIL
MAIL_SERVER ='smtp.gmail.com'
MAIL_PORT = 465
MAIL_USERNAME = 'awsomecrack3r@gmail.com'
MAIL_PASSWORD = 'AWSCrack3r'
MAIL_USE_TLS = False
MAIL_USE_SSL = True
SPAM = 'lucahc@hotmail.it'
