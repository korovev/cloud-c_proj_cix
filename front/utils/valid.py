import re

def mail(mail):
    if re.search("^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$", mail):
        return True
    return False

def hash(hash):
    if re.search("[a-zA-Z0-9.,_-]+\.txt", hash):
        return True
    return False
