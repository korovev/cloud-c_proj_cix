import os
import uuid
import ENV
from flask import render_template, request, redirect, url_for

def save(hash_file):
    hash_path = os.path.join(ENV.DATA, str(uuid.uuid4()))
    hash_file.save(hash_path)
    return hash_path
