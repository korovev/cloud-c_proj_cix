#!/usr/bin/env python

from flask import render_template, request, redirect, url_for, abort
from utils import valid, words, hash_saver
import os
import ENV
import requests
import json

from __init__ import app

words = os.listdir(ENV.WORDS)

@app.route("/", methods=['GET'])
def index():
    errors = []
    if request.args.get("errors"):
        errors = request.args.get("errors").split(";")
    return render_template('index.html', word_list = words, errors = errors)

@app.route("/error", methods=['GET'])
def error():
    return render_template('simple.html', data = "Errors lol")

@app.route("/send", methods=['POST'])
def send_to_back():
    errors = ""

    if not valid.hash(request.files["hash"].filename):
        errors += "Wrong hash filetype, it must be a txt file!" + ";"
    if not valid.mail(request.form["email"]):
        errors += "Wrong email format!" + ";"
    if errors != "":
        return redirect(url_for('index', errors = errors[:-1]))
    # Validation completed

    file = hash_saver.save(request.files['hash'])

    data = {
        'email':request.form["email"],
        'words':request.form.getlist('words')
    }

    try:
        response = requests.post(ENV.BACK_ADDR, data = data, files = {'hash': open(file, "rb")}, timeout = 10)
        response.raise_for_status()
    except requests.exceptions.Timeout as e:
        abort(503)
    except requests.exceptions.RequestException as e:
        abort(503)
    return render_template('simple.html', data = "We are processing the data!")
