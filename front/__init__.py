from flask import Flask

app = Flask(__name__)

import ENV
import index

app.config['DEBUG'] = ENV.DEBUG
app.config['ENV'] = 'development' if ENV.DEBUG else 'production'
app.config['TESTING'] = True if ENV.DEBUG else False


if __name__ == "__main__":
    if ENV.IS_PUBLIC_TEST:
        app.run(host="0.0.0.0", port = 80)
    else:
        app.run()
