#!/usr/bin/env python

from flask import render_template, request, redirect, url_for, abort
import os
import uuid
import multiprocessing
from utils import mail, job, valid, hash_saver, memory
import ENV

from __init__ import app

words = os.listdir(ENV.WORDS)

@app.route("/", methods=['POST'])
def start_decoding():
    dir(request)

    words = request.form.getlist("words")
    mail_addr = request.form["email"]
    wordpaths = []
    for word in words:
        wordpaths.append(os.path.join(ENV.WORDS, word))

    if  memory.used_perc() >= ENV.MEM_THRESHOLD:
        abort(503)
    hash_path, out_file = hash_saver.store(request.files['hash'])
    job.execute_parallel(hash_path, out_file, wordpaths, mail_addr)
    return ""
