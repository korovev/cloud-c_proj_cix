#!/usr/bin/env python

import sys
import logging

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,'/var/www/aws_flask')

from __init__ import app as application
