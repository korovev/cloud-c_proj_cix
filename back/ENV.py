#!/usr/bin/env python

import os

IS_LOCAL = False
IS_PUBLIC_TEST = False
# Apparently the home option doesn't work
PATH = "" if IS_LOCAL else "/var/www/aws_flask"

# RAM memory threshold for when to start dropping packets
MEM_THRESHOLD = 95

DEBUG = True
DATA = "/tmp"
WORDS = os.path.join(PATH, "words")

# EMAIL
MAIL_SERVER ='smtp.gmail.com'
MAIL_PORT = 465
MAIL_USERNAME = 'awsomecrack3r@gmail.com'
MAIL_PASSWORD = 'AWSCrack3r'
MAIL_USE_TLS = False
MAIL_USE_SSL = True
SPAM = 'lucahc@hotmail.it'
