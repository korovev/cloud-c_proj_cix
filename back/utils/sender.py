import ENV
from flask_mail import Mail, Message
from flask import render_template
from __init__ import app
import requests
# Abstract classes are not native in Python
# to implement them we need to implement this first!
# An abstract class needs to implement ABC!
from import abc import ABC, abstractmethod

# Abstract class for sender
class sender(ABC):

    _receiver = ""

    def __init__(self):
        super().__init__()

    def __init__(self, receiver):
        super().__init__()
        self.set_receiver(receiver)

    def set_receiver(receiver):
        self._receiver = receiver

    @abstractmethod
    def send_start(self):
        pass

    @abstractmethod
    def send_results(self, results):
        pass

    @abstractmethod
    def send_fail(self, error = "Generic Error"):
        pass

    @abstractmethod
    def send_no_match(self):
        pass

# Mock sender, it just do nothing
class mock_sender(sender):
    _receiver = ""
    def __init__(self, receiver):
        super().__init__(receiver)
    def send_start(self):
        print("Sending starting process!")
    def send_results(self, results):
        print("Sending results!")
    def send_fail(self, error = "Generic Error"):
        print("Sending an error!")
    def send_no_match(self):
        print("Sending a no match error!")

# Ping sender
class ping_sender(sender):
    _cmd = []
    def __init__(self, receiver):
        super().__init__(receiver)
        self._cmd = ["ping",
                "-c", "1",
                self._receiver]
    def send_start(self):
        sh.run(cmd)
    def send_results(self, results):
        sh.run(cmd)
    def send_fail(self, error = "Generic Error"):
        sh.run(cmd)
    def send_no_match(self):
        sh.run(cmd)

# Mail sender
class mail_sender(sender):
    flaskmail_sender = ""
    def __init__(self, receiver):
        super().__init__(receiver)
        # Setup mail server config
        app.config['MAIL_SERVER'] = ENV.MAIL_SERVER
        app.config['MAIL_PORT'] = ENV.MAIL_PORT
        app.config['MAIL_USERNAME'] = ENV.MAIL_USERNAME
        app.config['MAIL_PASSWORD'] = ENV.MAIL_PASSWORD
        app.config['MAIL_USE_TLS'] = ENV.MAIL_USE_TLS
        app.config['MAIL_USE_SSL'] = ENV.MAIL_USE_SSL
        self.flaskmail_sender = Mail(app)

    def send_start(self):
        msg = Message("Here we go!",
                        html = render_template('start.mail', receiver = self._receiver),
                        sender = ENV.MAIL_USERNAME,
                        recipients = [receiver])
        self.flaskmail_sender.send(msg)

    def send_results(self, results):
        msg = Message("Here we are!",
                        html = render_template('results.mail', results = results),
                        sender = ENV.MAIL_USERNAME,
                        recipients = [receiver])
        self.flaskmail_sender.send(msg)

    def send_fail(self, error = "Generic Error"):
        msg = Message("Oh, NO!",
                        html = render_template('fail.mail', receiver = self._receiver, error = error),
                        sender = ENV.MAIL_USERNAME,
                        recipients = [self._receiver])
        self.flaskmail_sender.send(msg)

    def send_no_match(self):
        msg = Message("No match!",
                        html = render_template('zeromatch.mail', receiver = self._receiver),
                        sender = ENV.MAIL_USERNAME,
                        recipients = [self._receiver])
        self.flaskmail_sender.send(msg)

class tele_sender(sender):
        _receiver = ""
        bot_token = '1372553874:AAF3uatKaWAmJWPiJZFU1Wi_BetEKbkrWUQ'
        bot_chatID = '328481715'   # CS in the '60s  -396467466
        send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text='

        def __init__(self, receiver):
            super().__init__(receiver)
            self._receiver = receiver

        def send_start(self):
            print("Sending starting process!")
        def send_results(self, results):
            print("Sending results!")
        def send_fail(self, error = "Generic Error"):
            print("Sending an error!")
        def send_no_match(self):
            print("Sending a no match error!")
