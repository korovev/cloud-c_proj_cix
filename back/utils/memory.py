import os
import subprocess as sh
def used_perc():
    mem_ava = sh.check_output("cat /proc/meminfo | grep MemAvailable: | awk '{print $2}'", shell=True)
    mem_tot = sh.check_output("cat /proc/meminfo | grep MemTotal: | awk '{print $2}'", shell=True)
    mem_ava = float(mem_ava)
    mem_tot = float(mem_tot)
    return ((mem_tot - mem_ava) / mem_tot) * 100
