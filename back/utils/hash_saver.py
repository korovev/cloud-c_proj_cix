import os
import uuid
import ENV
from flask import render_template, request, redirect, url_for

def store(hash_file):
    hash_path = os.path.join(ENV.DATA, str(uuid.uuid4()))
    hash_file.save(hash_path)
    outfile = hash_path + ".pot"
    return hash_path, outfile
