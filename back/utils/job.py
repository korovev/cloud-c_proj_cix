import os

import multiprocessing
import ENV

from utils import mail
from utils import hash

from utils import bot

def execute_parallel(hash_path, out_file, wordlists, receiver):
    proc = multiprocessing.Process(target=execute, args=(hash_path, out_file, wordlists, receiver))
    proc.start()

def execute(hash_path, out_file, wordlists, receiver):
    #mail.send_start(receiver)
    pairs = []
    try:
        pairs = hash.getall(hash_path, wordlists, out_file)
    except hash.HashException as e:
        #mail.send_fail(receiver, error = e.errors)
        bot.telegram_bot_sendtext(str(e.errors))
        return
    except hash.NoMatchException:
        #mail.send_no_match(receiver)
        bot.telegram_bot_sendtext("No Match!")
        return
    finally:
        if (os.path.exists(hash_path) and os.path.isfile(hash_path)):
            os.remove(hash_path)

    if ENV.DEBUG:
        print("The resulting hashes are:")
        print(pairs)
    #mail.send_results(receiver, pairs)
    bot.telegram_bot_sendtext(str(pairs))
