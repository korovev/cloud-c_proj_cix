import os
import ENV
import subprocess as sh
import uuid

class HashException(Exception):
    errors = ""
    def __init__(self, message):
        super().__init__(message)
        self.errors = message

class NoMatchException(Exception):
    pass

# It returns the list of pairs of hash:decoded_output.
def get(hashpath, wordspath, outfile, pairs=[]):
    cmd = ["hashcat",
            "--session", str(uuid.uuid4()),
            "-m", "0",
            "--force",
            hashpath,
            wordspath,
            "--potfile-path", outfile,
            "-O"]
    try:
        sh.run(cmd, check = True)
        # Hashcat stores the results on a file,
        # (there is nothing to do about it), so we need need to get the contents
        pairs = get_results(outfile)
    except sh.CalledProcessError as e:
        if e.returncode == 1:
            # Apparently hashcat returns 1 if didn't found all the hashes
            # If the outfile exists then he found some, if it doesn't it's because he found nothing
            if os.path.exists(outfile):
                pairs = get_results(outfile)
            else:
                raise NoMatchException("Zero Match")
        else:
            raise HashException("Hash cat has raised some errors: " + str(e.returncode) +"\n" + printhash(hashpath))
    finally:
        # If I don't remove the output file every time,
        # then hash cat won't compute the new hashes and instead retrieve the results in this file.
        if (os.path.exists(outfile) and os.path.isfile(outfile)):
            os.remove(outfile)
    if ENV.DEBUG:
        print("We got these hashes in this cycle:")
        print(pairs)
    return pairs

# Given a list of wordlist paths, it returns the pairs
def getall(hashpath, wordpaths, outfile, pairs=[]):
    for ele in wordpaths:
        try:
            pairs += get(hashpath, ele, outfile)
        except NoMatchException:
            pass

    if not pairs:
        raise NoMatchException("Zero Match")

    pairs = list(set(pairs))
    return pairs

def get_results(out_file):
    pairs=[]
    with open(out_file, "r") as results:
        for line in results:
            pairs.append(line)
    return pairs

def printhash(hash_path):
    rows=""
    with open(hash_path,"r") as hash_file:
        for line in hash_file:
            rows+=line
    return line
