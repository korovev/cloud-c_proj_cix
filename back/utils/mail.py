from flask_mail import Mail, Message
from flask import render_template

import ENV
from __init__ import app

# Setup mail server config
app.config['MAIL_SERVER'] = ENV.MAIL_SERVER
app.config['MAIL_PORT'] = ENV.MAIL_PORT
app.config['MAIL_USERNAME'] = ENV.MAIL_USERNAME
app.config['MAIL_PASSWORD'] = ENV.MAIL_PASSWORD
app.config['MAIL_USE_TLS'] = ENV.MAIL_USE_TLS
app.config['MAIL_USE_SSL'] = ENV.MAIL_USE_SSL

mail_sender = Mail(app)

def send_start(receiver):
    msg = Message("Here we go!",
                    html = render_template('start.mail', receiver = receiver),
                    sender = ENV.MAIL_USERNAME,
                    recipients = [receiver])
    mail_sender.send(msg)

def send_results(receiver, results):
    msg = Message("Here we are!",
                    html = render_template('results.mail', results = results),
                    sender = ENV.MAIL_USERNAME,
                    recipients = [receiver])
    mail_sender.send(msg)

def send_fail(receiver, error = "Generic Error"):
    msg = Message("Oh, NO!",
                    html = render_template('fail.mail', receiver = receiver, error = error),
                    sender = ENV.MAIL_USERNAME,
                    recipients = [receiver])
    mail_sender.send(msg)

def send_no_match(receiver):
    msg = Message("No match!",
                    html = render_template('zeromatch.mail', receiver = receiver),
                    sender = ENV.MAIL_USERNAME,
                    recipients = [receiver])
    mail_sender.send(msg)
