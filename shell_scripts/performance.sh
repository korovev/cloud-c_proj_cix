#!/bin/bash

targets=""
body=""
duration=0
rate=0
rate_adaption_mode=0 #default, rate is fixed at the value of ${rate}
from=1
to=2
adding=1

usage_show() {
    printf "Usage: $0 -t /path/to/targets -b /path/to/body [options]
Example: $0 -t data/targets.txt -b data/body.txt -d 120s -r 10 
Options:
-t|--targets : targets file
-b|--body : body file
--from : integer, initial starting rate
--to : integer, final goal rate
--adding : integer, rate accretion step
-d|--duration : integer, duration of the attack
-r|--rate : integer n, n attacks/second
-a|--adaption : integer [0,1]:
	0: default (fixed rate)
	1: increasing rate
NOTE!! -a|--adaption > 0 then -d|--duration is the duration of every single re-run, so be careful in choosing values too high or you will die waiting
"
}

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -t|--targets) targets="$2"; shift ;;
        -b|--body) body="$2"; shift ;;
        --from) from="$2"; shift ;;
        --to) to="$2"; shift ;;
        --adding) adding="$2"; shift ;;
        -d|--duration) duration="$2"; shift;;
        -r|--rate) rate="$2"; shift;;
        -a|--adaption) rate_adaption_mode="$2"; shift;;
        *) usage_show; exit 1
    esac
    shift
done

if [[ -z "${targets}" ]] || [[ -z "${body}"  ]]
then
    usage_show; exit 1
fi

dt=$(date '+%d%m%Y_%H_%M_%S')
if [[ "${rate_adaption_mode}" -eq 0 ]]
then
    vegeta attack -targets="${targets}" -body="${body}" -duration="${duration}" -rate="${rate}" |
    vegeta encode -to csv | 
    sed "s/\$/,$rate/" >> v_dump${dt}.csv
else
    for vrate in $(seq $from $adding $to); do
        vegeta attack -targets="${targets}" -body="${body}" -duration="${duration}" -rate="${vrate}" |
        vegeta encode -to csv | 
        sed "s/\$/,$vrate/" >> v_dump${dt}.csv
    done
fi
