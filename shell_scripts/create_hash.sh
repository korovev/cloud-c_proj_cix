#!/bin/sh

IFS=" ";
safe_mode=0

while getopts "sh" opt; do
	case ${opt} in
		s )
			safe_mode=1
			;;
		h )
			echo "Creates MD5 hashes of wordlists."
			echo "Usage:"
			echo "${0} [-s] [input-file] [output-file]"
			echo "-s: safe-mode, append instead of overwrite output file"
		    	exit 0
			;;
	esac
done
shift $((OPTIND -1))

#default file names
[ ! -z "$1" ] && input="$1" || input=clear.txt
[ ! -z "$2" ] && target="$2" || target=couples.txt

if [ $safe_mode == 1 ]; then
	[ -f "$target" ] || touch "$target"
else > "$target" #overwrite
fi

while read clear_text;
do
	if ! grep -xq "$clear_text" "$target";
	then
		hash="$(echo -n "$clear_text" | md5sum | awk '{print $1}')"
		printf "%s\n" "$hash" >> "$target"
		# printf "%s : %s\n" "$clear_text" "$hash" >> "$target"
	fi
done < "$input"
