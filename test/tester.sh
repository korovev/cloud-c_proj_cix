#!/bin/bash

now="$(date +'%d_%m_%Y')"
rates="6 8 12 16 22 26 30 15 28 35 50 70 20 15 80 90"
ratesfoo="1"
begin="$(date +'%d_%m_%Y_%H_%M')"

echo "#######################################        $(date +'%d_%m_%Y_%r')   BEGIN         ################################################" >> ./logs_tests/$now.log
printf "\n" >> ./logs_tests/$now.log
echo "Vegeta test using rates: [ ${rates} ] started at $(date +'%d_%m_%Y_%r') " >> ./logs_tests/$now.log
printf "\n" >> ./logs_tests/$now.log

for vrate in ${rates}; do
    printf "Starting attack with rate ${vrate} at $(date +'%d_%m_%Y_%r')\n\n" >> ./logs_tests/$now.log
    vegeta attack -targets=targets/targets.txt -body=targets/body_data.txt -duration=120s -rate="${vrate}" -timeout=30s | tee results.bin | vegeta report | tee ./.tmpf
    cat  ./.tmpf >> ./logs_tests/$now.log
    FILE="./vplot_rate${vrate}.html"
    if [ -f "${FILE}" ]; then
        occur_count=$(ls -d *rate${vrate}[!0-9]* | wc -l)
        occur_count=$((occur_count + 1))
        FILE="./vplot_rate${vrate}(iter${occur_count}).html"
    fi
    cat results.bin | vegeta plot > "${FILE}"
    printf "\n" >> ./logs_tests/$now.log
done
printf "\n" >> ./logs_tests/$now.log
echo "Vegeta test using rates: [ ${rates} ] terminated at $(date +'%d_%m_%Y_%r')" >> ./logs_tests/$now.log
printf "\n" >> ./logs_tests/$now.log
echo "#######################################################        END         ##############################################################" >> ./logs_tests/$now.log
printf "\n\n\n" >> ./logs_tests/$now.log

mkdir ./plots/"${begin}" && mv ./vplot_rate* ./plots/"${begin}"
