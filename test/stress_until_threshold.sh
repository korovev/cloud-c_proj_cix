#!/bin/sh

# Consts
now="$(date +'%d_%m_%Y')"
log_f="logs_tests/$now.log"
SEP='#######################################        %s   %s         ################################################\n\n'

# Params
duration=2m
timeout=30s
target="targets/targets.txt"
body="targets/body_data.txt"
ratesfoo=1
threshold=20
begin="$(date +'%d_%m_%Y_%H_%M')"
dir=plots/$now

start=6
inc=3

# 1: list of codes => "200:23 503:213 123:32 ..."
error_perc () {
    n_ok=0
    n_err=0
    perc=0
    [ "$1" = "" ] && echo 0 && return 0
    for pair in $1; do
        code=$(echo $pair | cut -d ':' -f 1)
        number=$(echo $pair | cut -d ':' -f 2)
        [ "$code" = 200 ] && let "n_ok += number" || let "n_err += number"
    done
    [ $n_ok = 0 ] && [ $n_err = 0 ] && echo 0 && return 0
    let "perc = n_err * 100 / (n_ok + n_err)"
    echo "$perc"
}

## Init
[ -d $dir ] || mkdir $dir

# Header
printf $SEP "$(date +'%d_%m_%Y_%r')" 'BEGIN' >> "$log_f"
printf "Vegeta test using rates: [ %d ] started at %s\n" "$rates" "$(date +'%d_%m_%Y_%r')">> "$log_f"

results=""
codes=""
vrate=$start
echo "Start..."
while [ "$(error_perc "$codes")" -lt "$threshold" ]; do
    printf "Starting attack with rate %d at %s\n" "$vrate" "$(date +'%d_%m_%Y_%r')" | tee "$log_f"
    FILE="vplot_rate$vrate.html"
    results="$(vegeta attack -targets=$target -body=$body -duration=$duration -rate="$vrate" -timeout=$timeout | tee results.bin | vegeta report)"
    echo -e "$results" >> "$log_f"
    codes="$(echo -e "$results" | grep 'Status Codes' | tr -s " " | cut -d " " -f 4- )"
    echo -e "My codes : $codes"
    if [ -f "$FILE" ]; then
        occur_count=$(ls -d *rate${vrate}[!0-9]* | wc -l)
        occur_count=$(let occur_count + 1)
        FILE="vplot_rate${vrate}(iter${occur_count}).html"
    fi
    cat results.bin | vegeta plot > "$dir/$FILE"
    let "vrate += inc"
    echo "new vrate $vrate"
done
printf "\n" >> "$log_f"
echo "Vegeta test using rates: [ ${rates} ] terminated at $(date +'%d_%m_%Y_%r')" >> "$log_f"
printf "\n" >> "$log_f"
printf $SEP '' 'END' >> "$log_f"
