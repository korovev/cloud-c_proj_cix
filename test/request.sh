#!/bin/bash

out=results/$(date '+%d%m%Y_%H_%M_%S')
mkdir $out

# 1: Name of the output file
# 2: Duration
# 3: Rate
function get_test {
  vegeta attack -targets=targets/get/targets.txt \
  -duration=${2} \
  -rate=${3} \
  -timeout=120s \
  | tee results.bin \
  | vegeta report
  cat results.bin | vegeta plot > $out/$1.html
}

echo "Starting the first phase..."
# First phase: Stress the GET requests
for vrate in $(seq 100 100 1000); do
  echo "Testing for $vrate packets per seconds..."
  get_test no_test_${vrate} 20s ${vrate}
  echo "Done."
done
echo "First phase finished"
exit 0

# Other_phases

for vrate in $(seq 8 3 17); do
    #if [ ${vrate} -ne 10 ]
    #then
    #    continue
    #fi
    vegeta attack -targets=targets/targets.txt \
      -body=targets/body_data.txt \
      -duration=90s \
      -rate="${vrate}" \
      -timeout=120s &
    get_test no_test_${vrate} 90s 50
    wait
done

#vegeta attack -targets=targets/targets.txt -body=targets/body_data.txt -duration=10s -rate=1 | tee results.bin | vegeta report

rm -f results.bin
